
# README #

### Purpose ###

* This small backend is part of a technical test for L'Atelier
* Version 0.1.0

### Set up ###

* clone the repository https://bitbucket.org/guirip/test-latelier-back/src/master/ : `git clone https://bitbucket.org/guirip/test-latelier-back.git` 
* install dependencies (`yarn` or `npm install`)
* start the server for development environment with command `yarn dev`

### Deployment ###

(Almost same as above)

* clone the repository
* install the dependencies from runtime only (`npm install "$package"`)
* start the server using pm2
`mode=production pm2 start path/to/src/index.js --name test-latelier-back`
* make sure the port configured for production in `src/config.js` file is allowed on the firewall
* save pm2 app list for automatic start at system (re)boot: `pm2 save`


#### To update deployed app

* Perform a SSH connection to the server
* Update the git repository (`git fetch`, `git rebase`)
* Restart the pm2 instance
    * use `pm2 list` and look for the name or id of this backend
    * then execute `pm2 restart id|name`

### Contact ###

* guirip @ gmail
