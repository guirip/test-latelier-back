const
  deepmerge = require('deepmerge'),
  ms = require('ms');


const DEFAULT_CONFIG = {
  port: process.env.mode === 'production' ? 3041 : 3001,
  appRoot : '',
  useHttps: false,

  tennisPlayersFeature: {
    dataUrl: 'https://latelierssl.blob.core.windows.net/trainingday/TennisStats/headtohead.json',
    dataValidity: ms('30s'),
  },
};


let localConfig;
try {
  localConfig = require('config.local');
} catch(e) {
  console.log('Default configuration is used');
  localConfig = {};
}

module.exports = deepmerge({}, DEFAULT_CONFIG, localConfig);
