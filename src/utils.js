
/**
 * Generic handler to end a request, sending data if there is any.
 */
function genericEndRequest(req, res) {
  if (!req.matched) {
    res.status(404).end();
  } else {
    res.status(200);
    if (req.data !== null && typeof req.data !== 'undefined') {
      res.json(req.data);
    }
    res.end();
  }
};

module.exports = {
  genericEndRequest,
}
