
const PATH = '/tennis-player';

const
  router = require('express').Router(),
  { genericEndRequest } = require('../utils');
  tennisPlayerService = require('../services/tennisPlayerService');


// GET all players
router.get('/', async function(req, res, next) {
  req.matched = true;

  const { players, error } = await tennisPlayerService.getAll();

  if (error || Array.isArray(players) !== true) {
    res.status(404);
    req.data = { error: error ? error.name : null };
  }
  else {
    players.sort(tennisPlayerService.sortPlayersByRank);

    req.data = {
      players,
    };
  }

  next();
});

// GET a single player
router.get('/:id', async function(req, res, next) {
  req.matched = true;

  if (typeof req.params.id !== 'string') {
    res.status(400);
    req.data = { error: `Missing 'id' argument` };
    return next();
  }

  const playerId = parseInt(req.params.id, 10);

  const { player, error } = await tennisPlayerService.get(playerId);

  if (error || !player) {
    res.status(404);
    req.data = { error: error ? error.name : null };
  }
  else {
    req.data = {
      player: player,
    };
  }

  next();
});

/**
 * Default handler to end all requests
 */
router.use(genericEndRequest);


module.exports = {
  path: PATH,
  router,
};