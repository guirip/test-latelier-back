
let
  config = require('./config'),
  app = require('express')(),
  logger = require('morgan'),
  bodyParser = require('body-parser'),
  compression = require('compression'),
  cors = require('cors'),
  shuttingDown = false;


function start() {

  const server = require('http').Server(app);

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(compression());
  app.use(logger('combined'));
  app.disable('x-powered-by');

  function addCorsHeaders(req, res) {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header(
      'Access-Control-Allow-Headers',
      'Content-Type, Authorization, Content-Length, X-Requested-With'
    );
  }

  // Set encoding for all response
  app.all(config.appRoot + '*', cors(), function(req, res, next) {
    res.charset = 'utf-8';
    next();
  });

  // Add routes
  const routes = [
    require('./routes/tennisPlayerRoute'),
  ];
  routes.forEach(function useRoute(route) {
    const path = config.appRoot + route.path;
    console.log(`Will manage routes on ${path}`);
    app.use(path, route.router);
  });

  // If no route matched, return HTTP status 404
  app.all('*', function(req, res) {
    res.status(404).end();
  });

  /**
   * Start server
   */
  var boot = function() {
    server.listen(config.port, function() {
      console.info(`${config.useHttps ? 'HTTPS' : 'HTTP'} server is running and listening on port ${config.port}`);
    });
  };

  /**
   * Shutdown server
   * @param {object} e
   * @param {number} code (exit code)
   */
  var stop = async function(e, code) {
    if (shuttingDown) {
      return;
    }
    shuttingDown = true;

    if (e && e.stack) {
      console.error(e.stack);
    }
    console.info('Shutdown...');

    await server.close();
    process.exit(code || 0);
  };

  // do something when app is closing
  process.on('exit', stop);

  // catch ctrl+c event
  process.on('SIGINT', stop);

  // catch uncaught exceptions
  process.on('uncaughtException', stop);


  boot();
}


start();