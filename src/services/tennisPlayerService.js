
const
  featureConfig = require('../config').tennisPlayersFeature,
  got = require('got');


let lastFetchedPlayers, lastFetchDate;


async function getRemoteData() {

  // Cache tennis players data to prevent performing too much requests
  if (lastFetchDate && new Date().getTime() - lastFetchDate < featureConfig.dataValidity) {
    return { players: lastFetchedPlayers };
  }

  let players, error;
  try {
    players = (await got(featureConfig.dataUrl).json()).players;
    lastFetchedPlayers = players;
    lastFetchDate = new Date().getTime();

  } catch(err) {
    console.error(`Cannot retrieve tennis players data - ${err.name} - ${err.response.body}`, err);
    error = err;
  }
  return { players, error };
}


async function getAll() {
  const { players, error } = await getRemoteData();
  return { players, error };
}

async function get(id) {
  const { players, error } = await getAll();
  return {
    player: Array.isArray(players) ? players.find(function(player) {
      return player.id === id;
    }) : null,
    error,
  }
}

function simpleNumericSort(valA, valB) {
  if (valA < valB) {
    return -1;
  }
  if (valB < valA) {
    return 1;
  }
  return 0;
}

const hasRank = player => player.data && typeof player.data.rank === 'number'

function sortPlayersByRank(playerA, playerB) {
  const
    playerAHasRank = hasRank(playerA),
    playerBHasRank = hasRank(playerB);

  if (playerAHasRank && playerBHasRank) {
    return simpleNumericSort(playerA.data.rank, playerB.data.rank);
  }
  // Put the players without rank at the end of the array
  if (!playerAHasRank) {
    return 1;
  }
  return -1; // playerB has no rank
}


module.exports = {
  getAll,
  get,
  sortPlayersByRank,
};
